# CS205-Final



## Scrabble! 

# Welcome to the Game Scrabble!
The rules are simple:
    -Before the game begins, all players should agree upon the dictionary that they will use, in this case we have done that for you with word validation. 
    -All words labeled as a part of speech (including those listed of foreign origin, and as archaic, obsolete, colloquial, slang, etc.) are permitted with the exception of the following: words always capitalized, abbreviations, prefixes and suffixes standing alone, words requiring a hyphen or an apostrophe.

    -Place all letters in the pouch, or facedown beside the board, and mix them up. (We've done this for you!)
    -The User goes first
    -All players draw seven new and place them on their racks.
    -The goal is to play valid words on the board and score more points than the computer
    -Drag and drop your tiles on to the board in a valid position to score!


# To Play:


