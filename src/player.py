import random

class Player:
    def __init__(self, bag, board):
        self.tiles = []
        self.bag = bag
        self.board = board
        self.consecPasses = 0
        self.points = 0
        for i in range(7):
            self.tiles.append(bag.pickTile())
        self.points = 0

    # method to refill player tiles from tiles in bag
    def refillTiles(self):
        for i in range(7-len(self.tiles)):
            if (len(self.bag.bag) > 0):
                self.tiles.append(self.bag.pickTile())
    
    # method to swap out player's tiles with tiles in bag
    def swapTiles(self,swaps):
        for tile in swaps:
            self.bag.bag.append(tile)
            if tile in self.tiles:
                self.tiles.remove(swaps[swaps.index(tile)])
                self.tiles.append(self.bag.pickTile())
        
        random.shuffle(self.bag.bag)

    # method to add points to the player's score
    def addPoint(self,pointsAdded):
        self.points += pointsAdded
    
    #method to print the tiles the player currently has
    def printCurrentTiles(self):
        print("Current Tiles: ", end = "")
        for tile in self.tiles:
            print(tile.letter, end = " ")
        print()

    def getTiles(self):
        return self.tiles
    
    # function that places a word given a list of the letters and corresponding coordinates
    def placeWord(self, word):
        # check word placement
        if self.board.checkPlacement(word) == True:
            # place word
            if isinstance(word[0][0], int):
                coords = word[0]
                self.board.board[coords[1]][coords[0]] = word[1]
            else:
                for i in range(len(word)):
                    coords = word[i][0]
                    self.board.board[coords[1]][coords[0]] = word[i][1]
            # check surroundings and calculate points
            points = self.board.checkSurroundings(word)
            # word placement is valid so refill tiles and add points to the player's score
            if points != -1:
                self.board.printBoard()
                self.consecPasses = 0
                for i in range(len(word)):
                    count = 0
                    for tile in self.tiles:
                        if tile.letter == word[i][1] and count == 0:
                            self.tiles.remove(tile)
                            count += 1
                self.refillTiles()
                self.points += points
                return self.points
            # word placement doesn't work so don't place word
            else:
                for i in range(len(word)):
                    coords = word[i][0]
                    self.board.board[coords[1]][coords[0]] = ' '
                self.board.printBoard()
                return -1
        # print word
        self.board.printBoard()
        return -1