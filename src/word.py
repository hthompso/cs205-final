
class Word:
    def __init__(self, word, points):
        self.word = word
        self.points = points

    # override less than comparison operator
    def __lt__(self, other):
        return self.points < other.points
        
    # override greater than comparison operator
    def __gt__(self, other):
        return self.points > other.points
    