# from numpy import char
from terminalBoard import Board
from words import Words
from player import Player
from computer import Computer
from bag import Bag


def playerTurn(player):
    valid = False
    while valid == False:
        player.printCurrentTiles()
        choice = input("exchange 1 / exchange all / place word / pass: ")
        if choice == "exchange all":
            player.swapTiles(player.tiles)
            player.consecPasses = 0
            valid = True
        elif choice == "exchange 1":
            exchange = input("Letter to exchange: ")
            swapped = False
            for tile in player.tiles:
                if swapped == False:
                    if tile.letter == exchange:
                        player.swapTiles([tile])
                        swapped = True
            player.consecPasses = 0
            valid = True
        elif choice == "place word":
            word = []
            num = int(input("Enter # of letters you will put down: "))
            print("For each letter, first enter the coordinates then the letter. ")
            for i in range(num):
                xcoord = -1
                while (xcoord < 0 or xcoord > 14):
                    try:
                        xcoord = int(input("Enter row coord: "))
                    except ValueError:
                        print("Please enter integer only.")
                ycoord = -1
                while (ycoord < 0 or ycoord > 14):
                    try:
                        ycoord = int(input("Enter column coord: "))
                    except ValueError:
                        print("Please enter integer only.")
                coords = [ycoord, xcoord]
                
                # Ken is still working on this error handling ...
                letter = input("Enter the letter: ")

                word.append([coords, letter])
            score = player.placeWord(word)
            if score != -1:
                player.consecPasses = 0
                valid = True
            else:
                print("Invalid word or placement.")
        elif choice == "pass":
            player.consecPasses += 1
            valid = True
        else: 
            print("Please enter valid input.")
            

def play():
    board = Board()
    tiles = Bag()
    words = Words()
    player = Player(tiles, board)
    level = input("Computer level ('Beginner', 'Intermediate', 'Advanced'): ")
    computer = Computer(tiles, words, level, board)
    gameOver = False
    board.printBoard()
    while gameOver == False:
        playerTurn(player)
        print("Your points: ",  player.points)
        if player.consecPasses == 2:
            gameOver = True
            print("You lost :(")
        elif len(player.tiles) == 0:
            gameOver = True
            print("You won!! :)")
        else:
            computer.playWord()
            print("Computer points: ",  computer.points)
                                                                                                                             