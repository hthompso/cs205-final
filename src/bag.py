from tile import Tile
import random

letters = {'A':[1,9], 'B':[3,2], 'C':[3,2], 'D':[2,4], 'E':[1,12], 
        'F':[4,2], 'G':[2,3], 'H':[4,2], 'I':[1,9], 'J':[8,1], 'K':[5,1], 'L':[1,4], 
        'M':[3,2], 'N':[1,6], 'O':[1,8], 'P':[3,2], 'Q':[10,1], 'R':[1,6], 'S':[1,4], 
        'T':[1,6], 'U':[1,4], 'V':[4,2], 'W':[4,2], 'X':[8,1], 'Y':[4,2], 'Z':[10,1]}

class Bag:
    def __init__(self) -> None:
        self.bag = []
        for letter in letters:
            for i in range(letters[letter][1]):
                self.bag.append(Tile(letter,letters[letter][0]))
        random.shuffle(self.bag)

    # pick tiles method
    def pickTile(self):
        pick = self.bag[0]
        self.bag.pop(0)
        return pick

    # def append(self,tile):
    #     self.bag.append(tile)