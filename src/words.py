import pathlib
from bag import *

class Words:
    def __init__(self) -> None:
        self.loadWords()
        
    # load words into dictionary from txt file
    def loadWords(self):
        path = str(pathlib.Path(__file__).parent.resolve()) + '/sowpods.txt'
        infile = open(path, "r")
        self.letterDict = {}
        word = infile.readline().rstrip()
        letters = ""
        newLetters = "AA"
        while word != "":
            if letters == newLetters:
                self.letterDict[letters].append(word)
            else:
                letters = newLetters
                self.letterDict[letters] = [word]
            word = infile.readline().rstrip()
            newLetters = word[0:2]
        infile.close()

    # method to validate that a word is in the dictionary
    def validateWord(self,word):
        letters = word[0:2]
        if letters in self.letterDict:
            if word in self.letterDict[letters]:
                    return True
            else:
                return False
        else:
            return False
    
    # method to calculate the score of a word
    def wordScore(self, word):
        score = 0
        for letter in word:
            score += letters[letter][0]
        return score
