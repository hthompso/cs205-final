from itertools import permutations #https://stackoverflow.com/questions/30159469/all-possible-combinations-of-a-word-python
from player import Player
from word import Word
from terminalBoard import Board
from words import Words
from bag import Bag

class Computer(Player):
    def __init__(self, bag, dict, level, board):
        super().__init__(bag, board)
        self.dict = dict
        self.words = []
        self.level = level

    def setLevel(self, level):
        self.level = level

    # method for computer to find all possible words that can be made with the letters existing on the board
    def findWords(self):
        letters = []
        for i in range(len(self.tiles)):
            letters.append(self.tiles[i].letter)
        self.wordList = []
        self.words = []
        # horizontal direction first:
        for i in range(len(self.board.board)):
            substr = ""
            #for each row check for tiles in row:
            for j in range(len(self.board.board)):
                if (self.board.board[i][j] != ' '):
                    substr += self.board.board[i][j]
                    if j+1 <15:
                        if (self.board.board[i][j+1] == ' '):
                            # find all words that contain the string and validate words
                            letters.append(substr)
                            for x in range(len(letters)+1):
                                for combination in permutations(letters, x): 
                                    if substr in combination:
                                        word = ''
                                        for letter in combination:
                                            word += letter
                                        if self.dict.validateWord(word) == True and word != substr:
                                            # figure out where the other letters would need to be placed around 
                                            # the substring
                                            self.words.append(word)
                                            substrIndex = word.index(substr)
                                            substrStart = j - len(substr) + 1
                                            letterList = []
                                            for index in range(len(word)):
                                                diff = substrIndex - index
                                                if index < substrIndex or index > substrIndex+len(substr)-1:
                                                    coords = [substrStart-diff, i]
                                                    letterList.append([coords, word[index]])
                                            letterList.append(word)
                                            self.wordList.append(letterList)
                            letters.pop(len(letters)-1)
        # vertical direction next:
        for i in range(len(self.board.board)):
            substr = ""
            #for each column check for tiles in column:
            for j in range(len(self.board.board)):
                if (self.board.board[j][i] != ' '):
                    substr += self.board.board[j][i]
                    if j + 1 < 15:
                        if (self.board.board[j+1][i] == ' '):
                            # find all words that contain the string and validate words
                            letters.append(substr)
                            for x in range(len(letters)+1):
                                for combination in permutations(letters, x): 
                                    if substr in combination:
                                        word = ''
                                        for letter in combination:
                                            word += letter
                                        if self.dict.validateWord(word) == True:
                                            # figure out where the other letters would need to be placed around 
                                            # the substring
                                            self.words.append(word)
                                            substrIndex = word.index(substr)
                                            substrStart = j - len(substr) + 1
                                            letterList = []
                                            for index in range(len(word)):
                                                diff = substrIndex - index
                                                if index < substrIndex or index > substrIndex+len(substr)-1:
                                                    coords = [i, substrStart-diff]
                                                    letterList.append([coords, word[index]])
                                            letterList.append(word)
                                            self.wordList.append(letterList)
                            letters.pop(len(letters)-1)

    # method to sort words in ascending order based on points each word would get
    def sortWords(self):
        # sort list of words
        sort = []
        for word in self.words:
            score = self.dict.wordScore(word)
            sort.append(Word(word,score))
        sort.sort()
        self.words.clear()
        for word in sort:
            self.words.append(word.word)
        # sort list of letters and corresponding coordinates
        sortedWordList = [[] for x in range(len(self.wordList))]
        for i in range(len(self.wordList)):
            elem = self.wordList[i][len(self.wordList[i])-1]
            index = self.words.index(elem)
            sortedWordList[index] = self.wordList[i]
            sortedWordList[index].pop(len(self.wordList[i])-1)
        self.wordList.clear()
        for index in range(len(sortedWordList)):
            if sortedWordList[index] != []:
                self.wordList.append(sortedWordList[index])

    # method for computer to pick a word from the wordlist based on the level that is set
    def pickWord(self):
        if self.level == "Beginner":
            word = self.wordList[0]
        elif self.level == "Intermediate":
            word = self.wordList[int(len(self.wordList)/2)]
        else:
            word = self.wordList[len(self.wordList)-1]
        self.wordList.remove(word) # remove word from word list
        return word

        
    # place word method for computer 
    def playWord(self):
        # call make words method
        self.findWords()
        # call sort words method and sort the wordlist too
        self.sortWords()
        # set chosen = False
        chosen = False
        # while chosen == False
        while chosen == False and len(self.wordList) > 0:
            # call pickword method
            word = self.pickWord()
            # if placeword(word) != -1
            wordScore = self.placeWord(word)
            if wordScore != -1:
                chosen = True
                # add points to computer score
                # self.points += wordScore
        # replace a tile instead of playing word
        if chosen == False and len(self.bag.bag) > 0:
            self.swapTiles([self.tiles[0]])
            self.consecPasses = 0
            return -1
        # pass the turn instead of playing word
        elif chosen == False:
            print("pass")
            self.consecPasses += 1
            if self.consecPasses >= 2:
                print("Game Over")
            return -1
        # won the game!
        elif len(self.bag.bag) == 0:
            print("You Won!!")
        # played the word
        else:
            self.consecPasses = 0
            return word

'''
bag = Bag()
dictionary = Words()
board = Board()
player = Player(bag, board)
print(player.placeWord([[[7,7], 'A'], [[8,7], 'N'], [[9,7], 'O'], [[10,7], 'T'], [[11,7], 'H'], [[12,7], 'E'], [[13,7], 'R']]))
print(player.placeWord([[[13,8], 'O'], [[13,9], 'W']]))
print(player.placeWord([[[14,9], 'H'], [[15,9], 'Y']]))
computer1 = Computer(bag, dictionary, "Advanced", board)
# bag.bag.clear()
print(computer1.points)
print(computer1.playWord())
print(computer1.points)
print(computer1.playWord())
print(computer1.points)
print(computer1.playWord())
print(computer1.points)
# print(computer1.playWord())
# print(computer1.points)
# print(computer1.playWord())
# print(computer1.points)
# print(computer1.playWord())
# print(computer1.points)
'''