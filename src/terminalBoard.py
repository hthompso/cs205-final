from words import Words

# locations for special tiles, in row, column order
doubleLetterScoreTiles = [[0,3], [0,11], [2,6], [2,8], [3,0], [3,7], [3,14], 
    [6,2], [6,6], [6,8], [6,12], [7,3], [7,11], [8,2], [8,6], [8,8], [8,12], 
    [11,0], [11,7], [11,14], [12,6], [12,8], [14,3], [14,11]]

doubleWordScoreTiles = [[1,1], [1,13], [2,2], [2,12], [3,3], [3,11], [4,4], [4,10], 
    [7,7], [10,4], [10,10], [11,3], [11,11], [12,2], [12,12], [13,1], [13,13]]

tripleLetterScoreTiles = [[1,5], [1,9], [5,1], [5,5], [5,9], 
    [5,13], [9,1], [9,5], [9,9], [9,13], [13,5], [13,9]]

tripleWordScoreTiles = [[0,0], [0,7], [0,14], [7,0], [7,14], [14,0], [14,7], [14,14]]

class Board:
    def __init__(self):
        # Structs a board in two-dimensional array.
        self.board = [[" " for i in range(15)] for j in range(15)]
        self.board[7][7] = "*"
        self.empty = True
        self.wordList = Words()
    

    # function that checks that a word placement is empty and first word goes through the star 
    # given a list of the letters and corresponding coordinates
    def checkPlacement(self, word):
        # check to make sure word fits on board
        for tile in word:
            if (tile[0][0] < 0 or tile[0][0] > 14) or (tile[0][1] < 0 or tile[0][1] > 14):
                return False
        if self.empty == True:
            # check if the word to put down is a valid word
            letters = ""
            # case where word is going right or down
            if word[0][0][0] < word[1][0][0] or word[0][0][1] < word[1][0][1]:
                for i in range(len(word)):
                    letters += word[i][1]
            # case where word is going left or up
            else:
                for i in range(len(word)):
                    letters += word[len(word)-i][1]
            if self.wordList.validateWord(letters) == False:
                return False
            # check that first word goes through star square
            onStar = False
            for i in range(len(word)):
                coordinate = word[i][0]
                if coordinate[0] == 7 and coordinate[1] == 7:
                    onStar = True
            if onStar == False:
                return onStar
            self.empty = False
        else:
            nextTo = False
            for i in range(len(word)):
                coordinate = word[i][0]
                # check that spaces are empty for letters to be put in board
                if self.board[coordinate[1]][coordinate[0]] != " ":
                    return False
                # check to make sure that it plays off a word already on the board
                if coordinate[1]-1 >= 0 and self.board[coordinate[1]-1][coordinate[0]] != ' ':
                    nextTo = True
                elif coordinate[1]+1 < 15 and self.board[coordinate[1]+1][coordinate[0]] != ' ':
                    nextTo = True
                elif coordinate[0]-1 < 15 and self.board[coordinate[1]][coordinate[0]-1] != ' ':
                    nextTo = True
                elif coordinate[0]+1 < 15 and self.board[coordinate[1]][coordinate[0]+1] != ' ':
                    nextTo = True
            if nextTo == False:
                return False
        return True
    
    # function to check to make sure the word works with surrounding words
    def checkSurroundings(self, list):
        turnWords = []
        turnScore = 0
        for i in range(len(list)):
            inputLetterCoords = list[i][0]
            # check for letter above
            if inputLetterCoords[1]-1 >= 0 and self.board[inputLetterCoords[1]-1][inputLetterCoords[0]] != ' ':
                # nextTo = True
                letter = self.board[inputLetterCoords[1]-1][inputLetterCoords[0]]
                coords = [inputLetterCoords[1]-1, inputLetterCoords[0]]
                # go left until no more letters
                while letter != ' ' and coords[0] > 0:
                    coords[0] = coords[0] - 1
                    letter = self.board[coords[0]][coords[1]]
                coords[0] = coords[0] + 1
                letter = self.board[coords[0]][coords[1]]
                word = letter
                # after finding first letter, add all letters to word and validate word
                while letter != ' ' and coords[0] < 14:
                    coords[0] = coords[0] + 1
                    letter = self.board[coords[0]][coords[1]]
                    if letter != ' ':
                        word += letter
                if self.wordList.validateWord(word) == False:
                    return -1
                else:
                    if word not in turnWords:
                        turnWords.append(word)
                        turnScore += self.wordList.wordScore(word)

            # check letter below
            elif inputLetterCoords[1]+1 < 15 and self.board[inputLetterCoords[1]+1][inputLetterCoords[0]] != ' ':
                # nextTo = True
                letter = self.board[inputLetterCoords[1]][inputLetterCoords[0]]
                coords = [inputLetterCoords[1], inputLetterCoords[0]]
                word = letter
                # add all letters to word and validate word
                while letter != ' ' and coords[0] < 14:
                    coords[0] = coords[0] + 1
                    letter = self.board[coords[0]][coords[1]]
                    if letter != ' ':
                        word += letter
                if self.wordList.validateWord(word) == False:
                    return -1
                else:
                    if word not in turnWords:
                        turnWords.append(word)
                        turnScore += self.wordList.wordScore(word)
            # check for letter to left
            if inputLetterCoords[0]-1 >= 0 and self.board[inputLetterCoords[1]][inputLetterCoords[0]-1] != ' ':
                # nextTo = True
                letter = self.board[inputLetterCoords[1]][inputLetterCoords[0]-1]
                coords = [inputLetterCoords[1], inputLetterCoords[0]-1]
                # go left until no more letters
                while letter != ' ' and coords[1] > 0:
                    coords[1] = coords[1] - 1
                    if coords[1] >= 0:
                        letter = self.board[coords[0]][coords[1]]
                    else:
                        letter = ' '
                coords[1] = coords[1] + 1
                letter = self.board[coords[0]][coords[1]]
                word = letter
                # after finding first letter, add all letters to word and validate word
                while letter != ' ' and coords[1] < 14:
                    coords[1] = coords[1] + 1
                    letter = self.board[coords[0]][coords[1]]
                    if letter != ' ':
                        word += letter
                if self.wordList.validateWord(word) == False:
                    return -1
                else:
                    if word not in turnWords:
                        turnWords.append(word)
                        turnScore += self.wordList.wordScore(word)
            # check letter to right
            elif inputLetterCoords[1]+1 < 15 and self.board[inputLetterCoords[1]][inputLetterCoords[0]+1] != ' ':
                # nextTo = True
                letter = self.board[inputLetterCoords[1]][inputLetterCoords[0]]
                coords = [inputLetterCoords[1], inputLetterCoords[0]]
                word = letter
                # add all letters to word and validate word
                while letter != ' ' and coords[1] < 14:
                    coords[1] = coords[1] + 1
                    letter = self.board[coords[0]][coords[1]]
                    if letter != ' ':
                        word += letter
                if self.wordList.validateWord(word) == False:
                    return -1
                else:
                    if word not in turnWords:
                        turnWords.append(word)
                        turnScore += self.wordList.wordScore(word)
        else:
            return turnScore

    def getBoard(self):
        # Returns the board in the string form.
        return self.board

    def printBoard(self):
        # Prints the board as a table in the terminal.
        boardString = "     " + "   ".join(str(item) for item in range(10)) + "   " + "  ".join(str(item) for item in range(10, 15)) + " "
        boardString += "\n     _   _   _   _   _   _   _   _   _   _   _   _   _   _   _ \n"
        board = list(self.board)
        for i in range(len(board)):
            if i < 10:
                board[i] = str(i) + "  | " + " | ".join(str(item) for item in board[i]) + " |"
            if i >= 10:
                board[i] = str(i) + " | " + " | ".join(str(item) for item in board[i]) + " |"
        boardString += "\n     _   _   _   _   _   _   _   _   _   _   _   _   _   _   _ \n".join(board)
        boardString += "\n     _   _   _   _   _   _   _   _   _   _   _   _   _   _   _ \n"
        print(boardString)

# some tests
# board = Board()
# print(board.placeWord([[[3,7], 'W'], [[4,7], 'O'], [[5,7], 'D'], [[6,7], 'D']]))
# print(board.placeWord([[[4,7], 'W'], [[5,7], 'O'], [[6,7], 'R'], [[7,7], 'D']]))
# print(board.placeWord([[[4,7], 'H'], [[5,7], 'E'], [[6,7], 'L'], [[7,7], 'L'], [[8,7], 'O']]))
# print(board.placeWord([[[7,6], 'A'], [[8,6], 'N'], [[9,6], 'D']]))