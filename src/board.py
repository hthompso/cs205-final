import pygame
import sys
import math

from player import Player
from bag import Bag
from words import Words
from terminalBoard import Board
from computer import Computer

from button import button

class Tile(pygame.sprite.Sprite):
    def __init__(self, letter, spritesheet, location):
        pygame.sprite.Sprite.__init__(self)
        letter.upper()
        self.image = spritesheet.imageAt(letter + ".png")

class ScrabbleBoard:
    
    def __init__(self):
        self.dictionary = Words()
        self.gameBag = Bag()
        self.terminalBoard = Board()
        self.player = Player(self.gameBag, self.terminalBoard)
        #self.computer = Computer(self.gameBag,self.dictionary,"Intermediate", self.terminalBoard)
        self.run = True
        self.tiles = [[None for _ in range(15)] for _ in range(15)]
        self.boardColor = (220,202,152)
        self.backgroundColor = (120,117,99)
        self.window = 1
        self.tileDrawings = []
   
    def runWindow(self):
        
        pygame.init()
        screen = pygame.display.set_mode((1200,850))
       
        pygame.display.set_caption("Scrabble")
        screen.fill(self.backgroundColor)
        

        pygame.display.flip()
        
        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                elif self.window == 1:
                    self.drawMainScreen(screen)
                elif self.window == 2:
                    self.drawBoard(screen)

    def drawBoard(self,screen):
        self.boardTiles = []
        boardColor = (220,202,152)
        for tilex in range(len(self.tiles)):
            for tiley in range(len(self.tiles[tilex])):
                if (self.tiles[tilex][tiley] is None):
                    tileRect = pygame.draw.rect(screen, boardColor, (tilex*50, tiley*50, 50, 50))
                    self.boardTiles.append(tileRect)
                    print(tileRect.center)
        
        pygame.draw.rect(screen,(255,255,255), (100,765,400,65))
        # draw the lines
        for i in range(15):
            pygame.draw.aaline(screen,
                                "black",
                                (0, i*50),
                                (750, i*50))
            pygame.draw.aaline(screen,
                                "black",
                                (i*50, 0),
                                (i*50, 750))
        
        # draw star in the middle
        star = pygame.image.load('../cs205-final/images/star.png')
        starTile = pygame.transform.scale(star, (50, 50))
        screen.blit(starTile,(350,350))

        # load bonus tiles
        doubleWord= pygame.image.load('../cs205-final/images/DoubleWord.png')
        tripleWord= pygame.image.load('../cs205-final/images/TripleWord.png')
        doubleLetter= pygame.image.load('../cs205-final/images/DoubleLetter.png')
        tripleLetter= pygame.image.load('../cs205-final/images/TripleLetter.png')
      
        # draw triple word tiles
        for x in range(0,750,350):
            for y in range (0,750,350):
                # if its the center tile, skip it
                if ((x,y) != (350,350)):
                    screen.blit(tripleWord,(x,y))

        # draw triple letter tiles
        for x in range(50,750,200):
            for y in range (50,750,200):
                if (((x,y) != (50,50)) and ((x,y) != (650,50)) and ((x,y) != (50,650)) and ((x,y) != (650,650))):
                    screen.blit(tripleLetter,(x,y))
        
        # draw double word tiles
        screen.blit(doubleWord,(50,50))
        screen.blit(doubleWord,(100,100))
        screen.blit(doubleWord,(150,150))
        screen.blit(doubleWord,(200,200))
        screen.blit(doubleWord,(500,500))
        screen.blit(doubleWord,(550,550))
        screen.blit(doubleWord,(600,600))
        screen.blit(doubleWord,(650,650))
        screen.blit(doubleWord,(50,650))
        screen.blit(doubleWord,(100,600))
        screen.blit(doubleWord,(150,550))
        screen.blit(doubleWord,(200,500))
        screen.blit(doubleWord,(500,200))
        screen.blit(doubleWord,(550,150))
        screen.blit(doubleWord,(600,100))
        screen.blit(doubleWord,(650,50))
        
        # draw double letter tiles
        for x in range(150, 750, 200):
            for y in range (0, 750, 700):
                if (((x,y) != (350,0)) and ((x,y) != (350,700))):
                    screen.blit(doubleLetter,(x,y))
        
        screen.blit(doubleLetter,(300,100))
        screen.blit(doubleLetter,(350,150))
        screen.blit(doubleLetter,(400,100))
        screen.blit(doubleLetter,(300,300))
        screen.blit(doubleLetter,(400,300))
        screen.blit(doubleLetter,(300,400))
        screen.blit(doubleLetter,(400,400))
        screen.blit(doubleLetter,(100,300))
        screen.blit(doubleLetter,(150,350))
        screen.blit(doubleLetter,(100,400))
        screen.blit(doubleLetter,(0,150))
        screen.blit(doubleLetter,(0,550))
        screen.blit(doubleLetter,(600,300))
        screen.blit(doubleLetter,(550,350))
        screen.blit(doubleLetter,(600,400))
        screen.blit(doubleLetter,(700,150))
        screen.blit(doubleLetter,(700,550))
        screen.blit(doubleLetter,(300,600))
        screen.blit(doubleLetter,(350,550))
        screen.blit(doubleLetter,(400,600))

        #Buttons! (Play) (Pass) (Replace)

        playWordButton = button((255,255,255), 915,100 ,140, 50, "Place")
        playWordButton.draw(screen)
        
        passButton = button((255,255,255), 915,200 ,140, 50, "Pass")
        passButton.draw(screen)

        replaceAllButton = button((255,255,255), 915,300 ,185, 50, "Replace All")
        replaceAllButton.draw(screen)
       
        running = True
        
        rect_moving = False
        clock = pygame.time.Clock()
        activeWords = []
        lettersOnBoard = []

        passAttempt = []
        FPS = 60
        

        while running == True:
            mouse = pygame.mouse.get_pos()
            
            self.drawRack(screen)
            BLACK = (0,0,0)

            font = pygame.font.SysFont('Helvetica', 18)
            
            for ev in pygame.event.get():
                    if ev.type == pygame.QUIT:
                        pygame.quit()
                        quit()
                    
                    elif ev.type == pygame.MOUSEMOTION:
                        
                        if playWordButton.isOver(mouse):
                            playWordButton.color = (173,216,130)
                            playWordButton.draw(screen)   
                            pygame.display.update() 

                        else:
                            playWordButton.color = (255,255,255)
                            playWordButton.draw(screen)    
                            pygame.display.update()

                        if passButton.isOver(mouse):
                            passButton.color = (173,216,130)
                            passButton.draw(screen)
                            pygame.display.update()   

                        else:
                            passButton.color = (255,255,255)
                            passButton.draw(screen)
                            pygame.display.update() 

                        if replaceAllButton.isOver(mouse):
                            replaceAllButton.color = (173,216,130)
                            replaceAllButton.draw(screen)
                            pygame.display.update()   

                        else:
                            replaceAllButton.color = (255,255,255)
                            replaceAllButton.draw(screen)
                            pygame.display.update()        

                    elif ev.type == pygame.MOUSEBUTTONDOWN:
                        if playWordButton.isOver(mouse):

                            passAttempt.clear()

                            self.player.printCurrentTiles() 
                            print(activeWords)
                            successPlace = self.player.placeWord(activeWords)

                            word = self.computer.playWord()
                            self.computerWordPlace(screen, word)

                            # Still working on this ... Might not be necessary.
                            # if(successPlace == -1):
                            activeWords.clear()

                        if passButton.isOver(mouse):

                            # Ends the game when the player tapped pass button twice.
                            if(len(passAttempt) == 1):
                                pygame.quit()
                                quit()
                            else:
                                passAttempt.append("1")

                            word = self.computer.playWord()
                            self.computerWordPlace(screen, word)

                        if replaceAllButton.isOver(mouse):

                            passAttempt.clear()
                            
                            print("All Tiles have been replaced!")
                            self.player.swapTiles(self.player.tiles)
                            self.player.printCurrentTiles()

                            word = self.computer.playWord()
                            self.computerWordPlace(screen, word)

                        for tile in self.tileDrawings:

                            if tile[0].collidepoint(ev.pos):
                                rect_moving = True
                                mouseX, mouseY = ev.pos
                                offsetX = tile[0].x - mouseX
                                offsetY = tile[0].y - mouseY
                                movingTile = tile
                                print(movingTile)

                    elif ev.type == pygame.MOUSEBUTTONUP:
                        if rect_moving == True:            
                            rect_moving = False
                            closest = self.closestRect(mouse)
                            lettersOnBoard.append(closest)
                            self.drawTile(screen,closest.center,movingTile[1])
                            activeWords.append([[int((closest.center[0]-25) / 50), int((closest.center[1]-25) / 50)], movingTile[1]])
                            # print([closest.center[0] / 50, closest.center[1] / 50])

                    if ev.type == pygame.MOUSEMOTION:
                        if rect_moving == True:
                            mouseX, mouseY = ev.pos
                            movingTile[0].x = mouseX + offsetX
                            movingTile[0].y = mouseY + offsetY
            
            # updating player and computer scores
            playerScore = font.render("Your Score: "+ str(self.player.points), True, BLACK)
            computerScore = font.render("Computer Score: " + str(self.computer.points) , True, BLACK)
            pygame.draw.rect(screen, self.backgroundColor, pygame.Rect(900,600,40,20))
            pygame.draw.rect(screen, self.backgroundColor, pygame.Rect(940,650,40,20))
            screen.blit(playerScore, (800, 600))
            screen.blit(computerScore, (800, 650))   

            pygame.display.update()

    def computerWordPlace(self, screen, word):
        for letter in word:
            yCoord = letter[0][0] * 50 + 25
            xCoord = letter[0][1] * 50 + 25
            self.drawTile(screen, (yCoord,xCoord) , letter[1])
    
    def closestRect(self, mouse):
        for i in self.boardTiles:
            xDis = (i.center[0] - mouse[0] - 25) ** 2
            yDis = (i.center[1] - mouse[1] - 25) ** 2
            distance = math.sqrt(xDis + yDis)
            if(distance < 50):
                return i

    def drawMainScreen(self,screen):
        pygame.init()
        
        play = True
        while play == True:
            width = screen.get_width()  
            height = screen.get_height()
            mouse = pygame.mouse.get_pos()

            font = pygame.font.SysFont('Helvetica', 18)
            title = pygame.font.SysFont('Helvetica', 30)
            BLACK = (0,0,0)
            header = title.render("Welcome to Scrabble!", True, (255,255,255))
            screen.blit(header, (465, 50))

            intro = font.render("How to Play", True, BLACK)
            screen.blit(intro, (550, 80))

            # game instructions
            i1 = font.render("There is a board and a bag of 98 letter tiles. Each player is dealt 7 letter tiles to start the game. You will always play first", True, BLACK)
            i2 = font.render("against the computer. There are three options each turn:", True, BLACK)
            i3 = font.render("1. Place a word", True, BLACK)
            i4 = font.render("2. Exchange all of your tiles (cannot play once they are replaced)", True, BLACK)
            i5 = font.render("3. Pass (2 passes in a row will result in a loss)", True, BLACK)
            i6 = font.render("Your first word MUST be placed on the star in the middle of the board. Each player will maintain 7 tiles on their rack.", True, BLACK)
            i7 = font.render("Create words and place them on the sqaures on the board by clicking on the tile, dragging it to the desired space, and releasing.", True, BLACK)
            i8 = font.render("Once you have placed all your letters, select the button PLACE and it will lock your word on the board.", True, BLACK)
            i9 = font.render("You can play off of other words that have been placed on the board as well.", True, BLACK)
            i10 = font.render("Different spots on the board have extra point values, which will multiply your word or letter score accordingly.", True, BLACK)
            i11 = font.render("Each player switches off placing a word until all tiles have been used, and the player with the highest score wins!", True, BLACK)
            i12 = font.render("Have fun!", True, BLACK)
            screen.blit(i1, (100, 150))
            screen.blit(i2, (100, 170))
            screen.blit(i3, (150, 190))
            screen.blit(i4, (150, 210))
            screen.blit(i5, (150, 230))
            screen.blit(i6, (100, 275))
            screen.blit(i7, (100, 295))
            screen.blit(i8, (100, 325))
            screen.blit(i9, (100, 345))
            screen.blit(i10, (100, 365))
            screen.blit(i11, (100, 385))
            screen.blit(i12, (550, 440))

            # playButton = button((255,0,0), width/2 - 65, height/2 + 60, 140, 50, "Play")
            # playButton.draw(screen)

            #computer level buttons
            beginnerButton = button((0, 105, 21), width/2 - 350, height/2 + 80, 160, 50, "Beginner")
            beginnerButton.draw(screen)

            intermediateButton = button((196, 196, 0), width/2 - 100, height/2 + 80, 200, 50, "Intermediate")
            intermediateButton.draw(screen)

            advancedButton = button((255, 0, 0), width/2 + 180, height/2 + 80, 180, 50, "Advanced")
            advancedButton.draw(screen)

            quitButton = button((255, 255, 255), width/2 - 65, height/2 + 200, 140, 50, "Quit")
            quitButton.draw(screen)

            # level and quit buttons for the menu screen
            for ev in pygame.event.get():
                if ev.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                
                elif ev.type == pygame.MOUSEMOTION:
                    
                    if beginnerButton.isOver(mouse):
                        beginnerButton.color = (0, 189, 38)
                        beginnerButton.draw(screen)
                        pygame.display.update() 

                    else:
                        beginnerButton.color = (0,105,21)
                        beginnerButton.draw(screen)    
                        pygame.display.update()

                    if intermediateButton.isOver(mouse):
                        intermediateButton.color = (255,255,0)
                        intermediateButton.draw(screen)   
                        pygame.display.update() 

                    else:
                        intermediateButton.color = (196,196,0)
                        intermediateButton.draw(screen)    
                        pygame.display.update()
                    
                    if advancedButton.isOver(mouse):
                        advancedButton.color = (255,0,0)
                        advancedButton.draw(screen)   
                        pygame.display.update() 

                    else:
                        advancedButton.color = (173, 0, 0)
                        advancedButton.draw(screen)    
                        pygame.display.update()

                    if quitButton.isOver(mouse):
                        quitButton.color = (184, 184, 184)
                        quitButton.draw(screen)
                        pygame.display.update()   

                    else:
                        quitButton.color = (255,255,255)
                        quitButton.draw(screen)
                        pygame.display.update()   

                elif ev.type == pygame.MOUSEBUTTONDOWN:
                    # switch computer level to beginner
                    if beginnerButton.isOver(mouse):
                        play = False
                        self.computer = Computer(self.gameBag,self.dictionary,"Beginner", self.terminalBoard)

                    # switch computer level to intermediate
                    if intermediateButton.isOver(mouse):
                        play = False
                        self.computer = Computer(self.gameBag,self.dictionary,"Intermediate", self.terminalBoard)

                    # switch computer level to advanced
                    if advancedButton.isOver(mouse):
                        play = False
                        self.computer = Computer(self.gameBag,self.dictionary,"Advanced", self.terminalBoard)
                    
                    # quit game
                    if quitButton.isOver(mouse):
                        pygame.quit()

        self.clearScreen(screen)
        self.drawBoard(screen)
        self.window = 2
            
    def clearScreen(self,screen):
        screen.fill((self.backgroundColor))

    # draw the user's tiles on their rack
    def drawRack(self,screen):
        rackCoord = 125
        font = pygame.font.SysFont('Helvetica', 18)
        BLACK = (0,0,0)
        for tile in self.player.tiles: 

            t= pygame.image.load('../cs205-final/images/' + tile.letter + '.png')
            
            #t = pygame.rect.Rect(rackCoord,765,50,50)
            #pygame.draw.rect(screen, (255,0,0), t)
            
            tileObj=t.get_rect()
            tileObj.center = rackCoord,800
            self.tileDrawings.append([tileObj,tile.letter])
            screen.blit(t,tileObj)
            rackCoord += 55

    # load in the scrabble tiles
    def drawTile(self,screen, coords, letter):        
        t = pygame.image.load('../cs205-final/images/' + letter + '.png')
        tile = t.get_rect()
        tile.center = coords[0],coords[1]
        self.tileDrawings.append([tile,letter])
        screen.blit(t,tile)
    
    

game = ScrabbleBoard()
game.runWindow()


