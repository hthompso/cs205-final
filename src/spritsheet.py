import pygame

class Spritesheet(object):
    def __init__(self,filename):
        try:
            self.sheet = pygame.image.load(filename).convet()
        except pygame.error as message:
            print('Unable to load spritesheet image:', filename)
            raise SystemExit(message)
    
    def imageAt(self, rectangle, colorkey = None):
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size).convert()
        image.blit(self.sheet, (0,0), rect)
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0,0))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image

    def imagesAt(self, rectangles, colorkey = None):
        return [self.image_at(rectangle, colorkey) for rectangle in rectangles]

    def loadStrip(self, rect, imageCount, colorkey = None):
        tups = [(rect[0]+rect[2]*x, rect[1], rect[2], rect[3]) for x in range(imageCount)]
        return self.images_at(tups, colorkey)
        
    