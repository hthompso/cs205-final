from terminalBoard import Board
from words import Words
from bag import Bag
from player import Player
from computer import Computer
from tile import Tile

def testWords():
    words = Words()
    # test validateWord method
    assert words.validateWord("HELLO") == True
    assert words.validateWord("JOADHJVS") == False
    # test wordScore method
    assert words.wordScore("THE") == 6
    assert words.wordScore("BANTER") == 8

def testBagInit():
    tiles = Bag()
    assert len(tiles.bag) == 98
    aCount = 0
    pCount = 0
    zCount = 0
    for i in range(98):
        if tiles.bag[i].letter == "A":
            assert tiles.bag[i].value == 1
            aCount += 1
        if tiles.bag[i].letter == "P":
            assert tiles.bag[i].value == 3
            pCount += 1
        if tiles.bag[i].letter == "Z":
            assert tiles.bag[i].value == 10
            zCount += 1
    assert aCount == 9
    assert pCount == 2
    assert zCount == 1

def testPlayer():
    # test player initialization
    bag = Bag()
    board = Board()
    player = Player(bag, board)
    assert len(player.tiles) == 7
    assert player.tiles[0].value > 0 and player.tiles[0].value <= 10
    assert len(bag.bag) == 91
    # test refillTiles method
    player.tiles.pop(2)
    player.tiles.pop(5)
    assert len(player.tiles) == 5
    player.refillTiles()
    assert len(player.tiles) == 7
    assert len(bag.bag) == 89
    # test swapTiles method
    newTile = bag.bag[0]
    oldTile = player.tiles[1]
    player.swapTiles([oldTile, player.tiles[5]])
    assert len(player.tiles) == 7
    assert newTile in player.tiles
    assert oldTile not in player.tiles
    assert newTile not in bag.bag
    assert oldTile in bag.bag
    # test player placeWord() method
    assert player.placeWord([[[3,7], 'W'], [[4,7], 'O'], [[5,7], 'R'], [[6,7], 'D']]) == -1
    assert player.board.board[3][7] == ' '
    assert player.placeWord([[[4,7], 'W'], [[5,7], 'O'], [[6,7], 'R'], [[7,7], 'D']]) == 8
    assert player.board.board[7][4] == 'W'
    assert player.board.board[7][7] == 'D'
    assert player.placeWord([[[4,7], 'H'], [[5,7], 'E'], [[6,7], 'L'], [[7,7], 'L'], [[8,7], 'O']]) == -1
    assert player.board.board[7][4] == 'W'
    assert player.board.board[7][7] == 'D'
    assert player.placeWord([[[9,7], 'H'], [[9,8], 'E'], [[9,9], 'L'], [[9,10], 'L'], [[9,11], 'O']]) == -1
    assert player.board.board[7][9] == ' '
    assert player.board.board[11][9] == ' '
    assert player.placeWord([[[5,3], 'H'], [[5,4], 'E'], [[5,5], 'L'], [[5,6], 'L']]) == 16
    assert player.board.board[3][5] == 'H'
    assert player.board.board[7][5] == 'O'
    board2 = Board()
    player2 = Player(bag, board2)
    assert player2.placeWord([[[7,7], 'A'], [[8,7], 'N'], [[9,7], 'O'], [[10,7], 'T'], [[11,7], 'H'], [[12,7], 'E'], [[13,7], 'R']]) != -1
    assert player2.placeWord([[[13,8], 'O'], [[13,9], 'W']]) != -1
    assert player2.placeWord([[[14,9], 'H'], [[15,9], 'Y']]) == -1
    

def testComputer():
    # test computer initialization
    bag = Bag()
    words = Words()
    board = Board()
    player = Player(bag, board)
    player.tiles[0] = Tile('A', 1)
    player.tiles[1] = Tile('N', 1)
    player.tiles[2] = Tile('D', 2)
    assert len(bag.bag) == 91
    computer = Computer(bag, words, "Beginner", board)
    assert len(computer.tiles) == 7
    assert computer.tiles[0].value > 0 and computer.tiles[0].value <= 10
    assert len(bag.bag) == 84
    # test playWords method for computer
    player.placeWord([[[7,7], 'A'], [[8,7], 'N'], [[9,7], 'D']])
    bagLength = 81
    assert len(bag.bag) == bagLength
    word = computer.playWord()
    assert word != -1
    points = computer.points
    assert points > 0
    bagLength -= len(word)
    assert len(bag.bag) == bagLength
    word = computer.playWord()
    assert word != -1
    assert computer.points > points
    bagLength -= len(word)
    assert len(bag.bag) == bagLength
    
# test board initialization
def testBoard():
    board = Board()
    assert len(board.board) == 15
    assert len(board.board[0]) == 15
    assert board.board[7][7] == '*'
